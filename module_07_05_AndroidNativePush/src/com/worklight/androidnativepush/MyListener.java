/*
* Licensed Materials - Property of IBM
* 5725-I43 (C) Copyright IBM Corp. 2006, 2013. All Rights Reserved.
* US Government Users Restricted Rights - Use, duplication or
* disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
*/
package com.worklight.androidnativepush;

import com.worklight.wlclient.api.WLClient;
import com.worklight.wlclient.api.WLEventSourceListener;
import com.worklight.wlclient.api.WLFailResponse;
import com.worklight.wlclient.api.WLOnReadyToSubscribeListener;
import com.worklight.wlclient.api.WLResponse;
import com.worklight.wlclient.api.WLResponseListener;

/** This class handles all callback functions for various actions such as 
 * a. onReadyToSubscribe
 * b. callback from connect
 * c. callback from subscribe
 * d. callback from unsubscribe
 * e. callback when a notification is received
 */
public class MyListener implements WLOnReadyToSubscribeListener, WLResponseListener, WLEventSourceListener{
	
	/* The mode defines what action the MyListener object will do */
	public static final int MODE_CONNECT = 0; 
	public static final int MODE_SUBSCRIBE = 1;
	public static final int MODE_UNSUBSCRIBE =2;
	
	private int mode ; 
	
	public MyListener(int mode){
		this.mode = mode; 
	}

	/* This function is called when the registration with GCM is successful. 
	 * We are now ready to subscribe and unsubscribe 
	 */
	@Override
	public void onReadyToSubscribe() {
	
		/* Register the event source callback for the alias of myAndroid. 
		 * This must be performed before we can subscribe or unsubscribe on an alias 
		 */
		WLClient.getInstance().getPush().registerEventSourceCallback("myAndroid", "PushAdapter","PushEventSource", this );
		
		AndroidNativePush.updateTextView("Ready to subscribe");
		AndroidNativePush.enableSubscribeButtons();
	}

	/* onFailure - Update the UI with the error message 
	 * 
	 */
	@Override
	public void onFailure(WLFailResponse arg0) {
		switch (mode){
		case MODE_CONNECT:
			AndroidNativePush.updateTextView("Unable to connect : " + arg0.getErrorMsg());
			break;
		
		case MODE_SUBSCRIBE:
			AndroidNativePush.updateTextView("Failure to subscribe : " + arg0.getErrorMsg());
			break;
			
		case MODE_UNSUBSCRIBE:
			AndroidNativePush.updateTextView("Failure to unsubscribe : " + arg0.getErrorMsg());
			break;
			
		}
	}

	
	@Override
	public void onSuccess(WLResponse arg0) {
		switch (mode){
		case MODE_CONNECT:
			AndroidNativePush.updateTextView("Connected successfully ");
			break;
		
		case MODE_SUBSCRIBE:
			AndroidNativePush.updateTextView("Subscribed successfully to push notifications");
			break;
			
		case MODE_UNSUBSCRIBE:
			AndroidNativePush.updateTextView("Unsubscribed successfully from push notifications");
			break;
			
		}				
	}

	/* Update the UI with the notification received */
	@Override
	public void onReceive(String arg0, String arg1) {
		AndroidNativePush.updateTextView("Notification received  " + arg0);	
	}

}
