/**
* Licensed Materials - Property of IBM
* 5725-I43 (C) Copyright IBM Corp. 2006, 2013. All Rights Reserved.
* US Government Users Restricted Rights - Use, duplication or
* disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
*/
package com.worklight.androidnativepush;

import java.util.HashMap;
import java.util.Map;

import android.util.Log;

import com.worklight.wlclient.api.WLFailResponse;
import com.worklight.wlclient.api.WLResponse;
import com.worklight.wlclient.api.challengehandler.ChallengeHandler;

public class SampleAppRealmChallengeHandler extends ChallengeHandler {
	public SampleAppRealmChallengeHandler(String realm, AndroidNativePush act) {
		super(realm);
	}

	@Override
	public boolean isCustomResponse(WLResponse response) {	
		Log.d("isCustomResponses", "isCustomResponse called");
		if (response == null || response.getResponseText() == null || 
				response.getResponseText().indexOf("j_security_check") == -1) {
			return false;
		}			
		return true;
	}

	@Override
	public void handleChallenge(WLResponse response) {
		Log.d("handleChallenge", "handleChallenge called");
		Map<String, String> params = new HashMap<String, String>();
		params.put("j_username", "worklight");
		params.put("j_password", "password");
		submitLoginForm("j_security_check", params, null, 0, "post");
	}
	
	public void onSuccess(WLResponse response) {
		Log.d("ChallengeHandler onSuccess", "onSuccess called");
		if(isCustomResponse(response)){
			Log.d("ChallengeHandler onSuccess", "Failed - wrong credentials");
			handleChallenge(response);
		}
		else{
			Log.d("ChallengeHandler onSuccess", "submitSuccess");
//			mainActivity.showLoginForm(View.GONE);
			submitSuccess(response);
		}
	}

	public void onFailure(WLFailResponse response) {
		Log.d("ChallengeHandler onFailure", "onFailure called");
	}	
}